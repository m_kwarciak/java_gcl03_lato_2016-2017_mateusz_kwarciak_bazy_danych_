package Program;

import CRUDs.UsersCRUD;
import Models.Alldata;
import Models.LoginHistory;
import Models.Roles;
import Models.Users;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mateusz on 27.05.2017.
 */
public class Main extends Application{

    public static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("myDatabase");
    public static EntityManager entityManager = entityManagerFactory.createEntityManager();

    public static void main(String[] args)
    {
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("myDatabase");
       // EntityManager entityManager = entityManagerFactory.createEntityManager();

        /*
        UsersCRUD usersCRUD = new UsersCRUD();

        Pattern pattern = Pattern.compile("[1-9][0-9][0-9][0-9]-[0-1][1-9]-[0-3][0-9] [0-2][0-9]:[0-6][0-9]:[0-6][0-9]");
        Matcher matcher = pattern.matcher("2017-05-27 19:36:18");
        boolean aaa = matcher.matches();
        System.out.print(aaa);
*/


        // entityManager.close();
       // entityManagerFactory.close();
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("/FXMLs/SignInScene.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Data Base");

        try {
            stage.getIcons().add(new Image(getClass().getResourceAsStream("icon.png")));

        }catch (Exception e) {System.out.println(e.getCause());}


        stage.show();

    }
}
