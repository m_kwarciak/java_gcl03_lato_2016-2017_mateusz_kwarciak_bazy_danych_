package CRUDs;

import Models.LoginHistory;
import Models.Users;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Mateusz on 27.05.2017.
 */
public class LoginHistoryCRUD {

    public void create(EntityManager entityManager, LoginHistory loginHistory)
    {
        entityManager.getTransaction().begin();

        entityManager.persist(loginHistory);

        entityManager.getTransaction().commit();
    }

    public List<LoginHistory> readAll(EntityManager entityManager)
    {
        List<LoginHistory> loginHistoryList = null;

        TypedQuery<LoginHistory> query = entityManager.createQuery("SELECT l from LoginHistory l", LoginHistory.class);
        loginHistoryList = query.getResultList();

        return loginHistoryList;
    }

    public void update(EntityManager entityManager, int id, LoginHistory data)
    {
        entityManager.getTransaction().begin();

        LoginHistory loginHistory = entityManager.find(LoginHistory.class, id);

        loginHistory.setTimestamp(data.getTimestamp());
        loginHistory.setUser(data.getUser());

        entityManager.getTransaction().commit();
    }

    public void delete(EntityManager entityManager, int id)
    {
        entityManager.getTransaction().begin();

        LoginHistory loginHistory = entityManager.find(LoginHistory.class, id);

        entityManager.remove(loginHistory);

        entityManager.getTransaction().commit();
    }
}
