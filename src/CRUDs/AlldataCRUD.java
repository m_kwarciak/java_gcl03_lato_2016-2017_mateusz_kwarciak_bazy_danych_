package CRUDs;

import Models.Alldata;
import Models.Users;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Mateusz on 27.05.2017.
 */
public class AlldataCRUD {

    public List<Alldata> readAll(EntityManager entityManager)
    {
        List<Alldata> alldataList = null;

        TypedQuery<Alldata> query = entityManager.createQuery("SELECT a from Alldata a", Alldata.class);
        alldataList = query.getResultList();

        return alldataList;
    }

}
