package Models;

import javax.persistence.*;

/**
 * Created by Mateusz on 27.05.2017.
 */
@Entity

public class Users {

    @Id
    @GeneratedValue
    private int idU;

    private String login;
    private String password;
    private String first_name;
    private String last_name;
    private int age;
    private String address;

    @OneToOne
    @JoinColumn(name = "roleID")
    private Roles roles;

    public int getIdU() { return idU; }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }
}