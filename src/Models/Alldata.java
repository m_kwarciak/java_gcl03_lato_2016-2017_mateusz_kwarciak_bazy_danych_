package Models;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;

import java.sql.Timestamp;

/**
 * Created by Mateusz on 27.05.2017.
 */
@Entity
@Immutable
@Table( name = "alldata")
public class Alldata {


    private String login;
    private String password;
    private String first_name;
    private String last_name;
    private int age;
    private String address;
    private String role;
    private int idR;
    private int userID;

    @Id
    private int idLH;

    private Timestamp timestamp;

    ////////////////////////////////////////

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public String getRole() {
        return role;
    }

    public int getIdR() {
        return idR;
    }

    public int getUserID() {
        return userID;
    }

    public int getIdLH() {
        return idLH;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public String toString()
    {
        return "LOGIN:" + login + "   PASSWORD: " + password + "   FIRST NAME:" + first_name +
                "   LAST NAME: " + last_name + "   AGE: " + age + "   ADDRESS: " + address +
                "   ROLE: " + role + "   ID_ROLE: " + idR + "   ID_USER: " + userID +
                "ID_LOGIN_HISTORY: " + idLH + "   TIME" + timestamp;
    }
}
