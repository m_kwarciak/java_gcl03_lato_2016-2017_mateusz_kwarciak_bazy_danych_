package Controllers;

import CRUDs.AlldataCRUD;
import CRUDs.RolesCRUD;
import CRUDs.UsersCRUD;
import Models.Alldata;
import Models.Roles;
import Models.Users;
import Program.Main;
import Validations.UsersValidation;
import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.sql.Timestamp;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Mateusz on 27.05.2017.
 */
public class DataBaseControllers implements Initializable{

    @FXML
    private ComboBox<String> userIdComboBox;

    @FXML
    private TextField loginField;

    @FXML
    private TextField passwordField;

    @FXML
    private ComboBox<String> roleComboBox;

    @FXML
    private TextField firstNameField;

    @FXML
    private TextField lastNameField;

    @FXML
    private TextField addressField;

    @FXML
    private TextField ageField;

    @FXML
    private Label userIDLabel;

    @FXML
    private Label loginLabel;

    @FXML
    private Label passwordLabel;

    @FXML
    private Label roleLabel;

    @FXML
    private Label firstNameLabel;

    @FXML
    private Label lastnameLabel;

    @FXML
    private Label addressLabel;

    @FXML
    private Label ageLabel;

    @FXML
    private Label statusLabel;

    @FXML
    private TableView<Alldata> table;

    @FXML
    private TableColumn<Alldata, String> loginColumn;

    @FXML
    private TableColumn<Alldata, String> passwordColumn;

    @FXML
    private TableColumn<Alldata, String> firstNameColumn;

    @FXML
    private TableColumn<Alldata, String> lastNameColumn;

    @FXML
    private TableColumn<Alldata, Integer> ageColumn;

    @FXML
    private TableColumn<Alldata, String> addressColumn;

    @FXML
    private TableColumn<Alldata, String> roleColumn;

    @FXML
    private TableColumn<Alldata, Integer> idRoleColumn;

    @FXML
    private TableColumn<Alldata, Integer> userIDColumn;

    @FXML
    private TableColumn<Alldata, Integer> idLoginHistoryColumn;

    @FXML
    private TableColumn<Alldata, Timestamp> timeStampColumn;

    private List<Alldata> alldataList;
    private Roles roleChoice = null;
    private Users userChoice = null;


    @FXML
    void exitAction(ActionEvent event) {
        Main.entityManager.close();
        Main.entityManagerFactory.close();
    }

    @FXML
    void updateUserAction(ActionEvent event) {

        UsersValidation usersValidation = new UsersValidation();
        Users user = new Users();

        boolean permission = true;

        if(usersValidation.loginValidation(loginField.getText())){
            user.setLogin(loginField.getText());
        }else{
            permission = false;
            loginLabel.setText("Wrong login");
        }

        if(usersValidation.passwordValidation(passwordField.getText())){
            user.setPassword(passwordField.getText());
        }else{
            permission = false;
            passwordLabel.setText("Wrong password");
        }

        if(usersValidation.nameValidation(firstNameField.getText())){
            user.setFirst_name(firstNameField.getText());
        }else{
            permission = false;
            firstNameLabel.setText("Wrong first name");
        }

        if(usersValidation.nameValidation(lastNameField.getText())){
            user.setLast_name(lastNameField.getText());
        }else{
            permission = false;
            lastnameLabel.setText("Wrong last name");
        }

        if(usersValidation.ageValidation(ageField.getText())){
            int age = Integer.parseInt(ageField.getText());
            user.setAge(age);
        }else{
            permission = false;
            ageLabel.setText("Wrong age");
        }

        if(usersValidation.addressValidation(addressField.getText())){
            user.setAddress(addressField.getText());
        }else{
            permission = false;
            addressLabel.setText("Wrong address");
        }

        if(roleChoice!=null) {
            user.setRoles(roleChoice);
        }else {
            permission = false;
            roleLabel.setText("Wrong role");
        }

        if(userChoice!=null) {}
        else {
            permission = false;
            userIDLabel.setText("Wrong role");
        }

        if(permission)
        {
            UsersCRUD usersCRUD = new UsersCRUD();
            usersCRUD.update(Main.entityManager, userChoice.getIdU(), user);
            statusLabel.setText("Success");
        }
        else {
            statusLabel.setText("Error");
        }


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        AlldataCRUD alldataCRUD = new AlldataCRUD();
        alldataList = alldataCRUD.readAll(Main.entityManager);

        ObservableList<Alldata> obList = FXCollections.observableList(alldataList);


        loginColumn.setCellValueFactory(new PropertyValueFactory<Alldata, String>("login"));
        passwordColumn.setCellValueFactory(new PropertyValueFactory<Alldata, String>("password"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Alldata, String>("first_name"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Alldata, String>("last_name"));
        ageColumn.setCellValueFactory(new PropertyValueFactory<Alldata, Integer>("age"));
        addressColumn.setCellValueFactory(new PropertyValueFactory<Alldata, String>("address"));
        roleColumn.setCellValueFactory(new PropertyValueFactory<Alldata, String>("role"));
        idRoleColumn.setCellValueFactory(new PropertyValueFactory<Alldata, Integer>("idR"));
        ageColumn.setCellValueFactory(new PropertyValueFactory<Alldata, Integer>("age"));
        userIDColumn.setCellValueFactory(new PropertyValueFactory<Alldata, Integer>("userID"));
        idLoginHistoryColumn.setCellValueFactory(new PropertyValueFactory<Alldata, Integer>("idLH"));
        timeStampColumn.setCellValueFactory(new PropertyValueFactory<Alldata, Timestamp>("timestamp"));

        table.setItems(obList);

        /////////////////////////////////////////////////////////////////////////////////////////

        RolesCRUD rolesCRUD = new RolesCRUD();
        List<Roles> rolesList = rolesCRUD.readAll(Main.entityManager);

       for(Roles el: rolesList)
        {
            roleComboBox.getItems().add(el.getRole());
        }

        roleComboBox.getSelectionModel().selectedItemProperty()
                .addListener(new ChangeListener<String>() {
                    public void changed(ObservableValue<? extends String> observable,
                                        String oldValue, String newValue) {
                        System.out.println("Value is: "+newValue);

                        for(Roles el: rolesList)
                        {
                            if(el.getRole().equals(newValue))
                                roleChoice = el;
                        }
                    }
                });
        /////////////////////////////////////////////////////////////////////////////////////
        //userIdComboBox

        UsersCRUD usersCRUD = new UsersCRUD();
        List<Users> usersList = usersCRUD.readAll(Main.entityManager);

        for(Users el: usersList)
        {
            String tmp = new Integer(el.getIdU()).toString();
            userIdComboBox.getItems().add(tmp);
        }

        userIdComboBox.getSelectionModel().selectedItemProperty()
                .addListener(new ChangeListener<String>() {
                    public void changed(ObservableValue<? extends String> observable,
                                        String oldValue, String newValue) {
                        System.out.println("Value is: "+newValue);

                        for(Users el: usersList)
                        {
                            String tmp = new Integer(el.getIdU()).toString();
                            if(tmp.equals(newValue))
                            {
                                userChoice = el;

                                loginField.setText(el.getLogin());
                                passwordField.setText(el.getPassword());
                                firstNameField.setText(el.getFirst_name());
                                lastNameField.setText(el.getLast_name());
                                ageField.setText(new Integer(el.getAge()).toString());
                                addressField.setText(el.getAddress());

                            }
                        }
                    }
                });


    }
}