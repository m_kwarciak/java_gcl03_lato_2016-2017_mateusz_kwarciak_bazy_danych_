package Controllers;

import CRUDs.LoginHistoryCRUD;
import CRUDs.UsersCRUD;
import Models.LoginHistory;
import Models.Users;
import Program.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Mateusz on 27.05.2017.
 */

public class SignInController implements Initializable{

    //private EntityManagerFactory entityManagerFactory;
    //private EntityManager entityManager;


    @FXML
    private TextField loginField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Label loginLabel;

    @FXML
    private Label passwordLabel;

    @FXML
    void signInAction(ActionEvent event) throws IOException {

        //entityManagerFactory = Persistence.createEntityManagerFactory("myDatabase");
        //entityManager = entityManagerFactory.createEntityManager();

        UsersCRUD usersCRUD = new UsersCRUD();
        List<Users> usersList = usersCRUD.readAll(Main.entityManager);

        for(Users el: usersList)
        {
            if(el.getLogin().equals(loginField.getText()))
            {
                if(el.getPassword().equals(passwordField.getText()))
                {
                    Calendar calendar = Calendar.getInstance();
                    Date now = calendar.getTime();
                    Timestamp timestamp = new Timestamp(now.getTime());

                    LoginHistory loginHistory = new LoginHistory();
                    loginHistory.setUser(el);
                    loginHistory.setTimestamp(timestamp);

                    LoginHistoryCRUD loginHistoryCRUD = new LoginHistoryCRUD();
                    loginHistoryCRUD.create(Main.entityManager, loginHistory);


                    Parent home_page_parent = FXMLLoader.load(getClass().getResource("../FXMLs/DataBaseScene.fxml"));
                    Scene home_page_scene = new Scene(home_page_parent);
                    Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();


                    app_stage.setScene(home_page_scene);
                    app_stage.show();
                }
                else
                {
                    passwordLabel.setText("Wrong password");
                }
            }
        }

        loginLabel.setText("Wrong login");

        //entityManager.close();
        //entityManagerFactory.close();
    }

    @FXML
    void signUpAction(ActionEvent event) throws IOException {

        Parent home_page_parent = FXMLLoader.load(getClass().getResource("../FXMLs/SignUpScene.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();


        app_stage.setScene(home_page_scene);
        app_stage.show();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
