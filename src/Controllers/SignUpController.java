package Controllers;

import CRUDs.RolesCRUD;
import CRUDs.UsersCRUD;
import Models.Roles;
import Models.Users;
import Program.Main;
import Validations.UsersValidation;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.persistence.criteria.CriteriaBuilder;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Mateusz on 27.05.2017.
 */
public class SignUpController implements Initializable {

    @FXML
    private TextField loginField;

    @FXML
    private TextField passwordField;

    @FXML
    private TextField fisrtNameField;

    @FXML
    private TextField lastNamField;

    @FXML
    private TextField ageField;

    @FXML
    private TextField addressField;

    @FXML
    private ComboBox<String> comboBox;

    @FXML
    private Label loginLabel;

    @FXML
    private Label passwordLabel;

    @FXML
    private Label firstNameLabel;

    @FXML
    private Label lastNameLabel;

    @FXML
    private Label ageLabel;

    @FXML
    private Label addressLabel;

    @FXML
    private Label choiceBoxLabel;

    @FXML
    private Label statusLabel;

    private Roles roleChoice = null;

    @FXML
    void backAction(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("../FXMLs/SignInScene.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();


        app_stage.setScene(home_page_scene);
        app_stage.show();

    }

    @FXML
    void signUpAction(ActionEvent event) {

        UsersValidation usersValidation = new UsersValidation();
        Users user = new Users();

        boolean permission = true;

        if(usersValidation.loginValidation(loginField.getText())){
            user.setLogin(loginField.getText());
        }else{
            permission = false;
            loginLabel.setText("Wrong login");
        }

        if(usersValidation.passwordValidation(passwordField.getText())){
            user.setPassword(passwordField.getText());
        }else{
            permission = false;
            passwordLabel.setText("Wrong password");
        }

        if(usersValidation.nameValidation(fisrtNameField.getText())){
            user.setFirst_name(fisrtNameField.getText());
        }else{
            permission = false;
            firstNameLabel.setText("Wrong first name");
        }

        if(usersValidation.nameValidation(lastNamField.getText())){
            user.setLast_name(lastNamField.getText());
        }else{
            permission = false;
           lastNameLabel.setText("Wrong last name");
        }

        if(usersValidation.ageValidation(ageField.getText())){
            int age = Integer.parseInt(ageField.getText());
            user.setAge(age);
        }else{
            permission = false;
            ageLabel.setText("Wrong age");
        }

        if(usersValidation.addressValidation(addressField.getText())){
            user.setAddress(addressField.getText());
        }else{
            permission = false;
            addressLabel.setText("Wrong address");
        }

        if(roleChoice!=null) {
            user.setRoles(roleChoice);
        }else {
            permission = false;
            choiceBoxLabel.setText("Wrong role");
        }

        if(permission)
        {
            UsersCRUD usersCRUD = new UsersCRUD();
            usersCRUD.create(Main.entityManager, user);
            statusLabel.setText("Success");
        }


    }

    @FXML
    void resetAction(ActionEvent event) {
        loginLabel.setText("");
        passwordLabel.setText("");
        firstNameLabel.setText("");
        lastNameLabel.setText("");
        ageLabel.setText("");
        addressLabel.setText("");
        choiceBoxLabel.setText("");

        statusLabel.setText("");

        loginField.clear();
        passwordField.clear();
        fisrtNameField.clear();
        lastNamField.clear();
        ageField.clear();
        addressField.clear();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        RolesCRUD rolesCRUD = new RolesCRUD();
        List<Roles> rolesList = rolesCRUD.readAll(Main.entityManager);

        for(Roles el: rolesList)
        {
            comboBox.getItems().add(el.getRole());
        }

        comboBox.getSelectionModel().selectedItemProperty()
                .addListener(new ChangeListener<String>() {
                    public void changed(ObservableValue<? extends String> observable,
                                        String oldValue, String newValue) {
                        System.out.println("Value is: "+newValue);

                        for(Roles el: rolesList)
                        {
                            if(el.getRole().equals(newValue))
                                roleChoice = el;
                        }
                    }
                });
    }
}