package Validations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mateusz on 27.05.2017.
 */
public class LoginHistoryValidation {

    public boolean roleValidation(String data)
    {
        boolean result;

        Pattern pattern = Pattern.compile("[1-9][0-9][0-9][0-9]-[0-1][1-9]-[0-3][0-9] [0-2][0-9]:[0-6][0-9]:[0-6][0-9]");
        Matcher matcher = pattern.matcher(data);
        result = matcher.matches();

        return result;
    }
}
