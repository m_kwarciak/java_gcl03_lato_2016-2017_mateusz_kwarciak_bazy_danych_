package Validations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mateusz on 27.05.2017.
 */
public class RolesValidation {

    public boolean roleValidation(String role)
    {
        boolean result;

        Pattern pattern = Pattern.compile("[a-z]{1,255}");
        Matcher matcher = pattern.matcher(role);
        result = matcher.matches();

        return result;
    }
}
